package config

import (
	"bobobox_assignment/exception"
	"context"
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func NewMySqlDatabase() *sql.DB {
    db, err := sql.Open("mysql", "admin:bismillah123@tcp(localhost:3306)/bobobox_asignment?parseTime=true")
    exception.PanicIfErr(err)
	
	return db
}

func NewMySqlContext() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 10*time.Second)
}

func NewNullString(s string) sql.NullString {
    if len(s) == 0 {
        return sql.NullString{}
    }
    return sql.NullString{
        String: s,
        Valid: true,
    }
}
