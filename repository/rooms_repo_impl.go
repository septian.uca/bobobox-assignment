package repository

import (
	"bobobox_assignment/config"
	"bobobox_assignment/model"
	"database/sql"
)

type roomsRepoImpl struct {
	sqlDb			*sql.DB
}

func NewRoomsRepo(database *sql.DB) RoomsRepo {
	return &roomsRepoImpl{
		sqlDb:                  database,
	}
}

func (repo *roomsRepoImpl) GetRoomAvailability(req model.AvailabilityReq)(res []model.AvailableRoomRepo, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()
	
	query := "SELECT r.id as room_id, r.room_number, p.price, p.`date` FROM room as r INNER JOIN price as p on p.room_type_id = r.room_type_id LEFT JOIN stay_room sr ON sr.room_id = r.id AND (sr.`date` >= ? OR sr.`date` <= ?) WHERE r.room_type_id = ? AND r.room_status = 'available' AND sr.room_id IS NULL ORDER BY r.id ASC ";

	record, err := repo.sqlDb.QueryContext(ctx, query, req.CheckinDate, req.CheckoutDate, req.RoomTypeId)
	if err != nil {
		return nil, err
	}
	for record.Next() {
		var RoomId int64
		var RoomNumber int16
		var Price float64
		var Date string

		err = record.Scan(&RoomId, &RoomNumber, &Price, &Date)
		if err != nil {
			return nil, err
		}

		res = append(res, model.AvailableRoomRepo{
			RoomId: 			RoomId,
			RoomNumber: 		RoomNumber,
			Price: 			Price,
			Date: 			Date,
		})
	}

	return res, err
}

func (repo *roomsRepoImpl) CreateRoom(req model.Room)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "INSERT INTO room(hotel_id, room_type_id, room_number, room_status) VALUES(?, ?, ?, ?)"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.HotelId, req.RoomTypeId, req.RoomNumber, req.RoomStatus)
	return err
}

func (repo *roomsRepoImpl) CreateRoomType(name string)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "INSERT INTO room_type (name) VALUES(?)"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, name)
	return err
}

func (repo *roomsRepoImpl) CreateRoomPrice(req model.PriceCreate)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()
	
	query := "INSERT INTO price (date, room_type_id, price) VALUES (?, ?, ?)"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.Date, req.RoomTypeId, req.Price)
	return err
}

func (repo *roomsRepoImpl) UpdateRoom(req model.RoomUpdate)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE room SET hotel_id = ?, room_type_id = ?, room_number = ?, room_status = ? WHERE id = ?"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.HotelId, req.RoomTypeId, req.RoomNumber, req.RoomStatus, req.Id)
	return err
}

func (repo *roomsRepoImpl) UpdateRoomType(req model.RoomType)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE room_type SET name = ? WHERE id = ?"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.Name, req.Id)
	return err
}

func (repo *roomsRepoImpl) UpdateRoomPrice(req model.Price)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE price SET date = ?, room_type_id = ?, price = ? WHERE id = ?"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.Date, req.RoomTypeId, req.Price, req.Id)
	return err
}

func (repo *roomsRepoImpl) RoomTypeById(id int8)(res model.RoomType, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, name FROM room_type WHERE id= ?"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id)
	err = row.Scan(&res.Id, &res.Name)

	return res, err
}

func (repo *roomsRepoImpl) RoomById(id int64)(res model.RoomUpdate, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, hotel_id, room_number, room_status, room_type_id FROM room WHERE id= ?"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id)
	err = row.Scan(&res.Id, &res.HotelId, &res.RoomNumber, &res.RoomStatus, &res.RoomTypeId)

	return res, err
}

func (repo *roomsRepoImpl) RoomPriceById(id int64)(res model.Price, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, date, price, room_type_id FROM price WHERE id= ?"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id)

	err = row.Scan(&res.Id, &res.Date, &res.Price, &res.RoomTypeId)
	return res, err
}
