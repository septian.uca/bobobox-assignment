package repository

import "bobobox_assignment/model"

type RoomsRepo interface {
	GetRoomAvailability(req model.AvailabilityReq)(res []model.AvailableRoomRepo, err error)
	CreateRoom(req model.Room)(err error)
	CreateRoomType(name string)(err error)
	CreateRoomPrice(req model.PriceCreate)(err error)
	UpdateRoom(req model.RoomUpdate)(err error)
	UpdateRoomType(req model.RoomType)(err error)
	UpdateRoomPrice(req model.Price)(err error)
	RoomTypeById(id int8)(res model.RoomType, err error)
	RoomById(id int64)(res model.RoomUpdate, err error)
	RoomPriceById(id int64)(res model.Price, err error)
}