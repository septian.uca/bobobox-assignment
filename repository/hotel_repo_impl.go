package repository

import (
	"bobobox_assignment/config"
	"bobobox_assignment/model"
	"database/sql"
)

type hotelRepoImpl struct {
	sqlDb			sql.DB
}

func NewHotelRepo(sqlDb *sql.DB) HotelRepo {
	return &hotelRepoImpl{
		sqlDb: 			*sqlDb,
	}
}

func (repo *hotelRepoImpl) Create(req model.Hotel)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "INSERT INTO hotel(hotel_name, address) VALUES(?, ?)"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.HotelName, req.Address)
	return err
}

func (repo *hotelRepoImpl) Update(req model.HotelUpdate)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE hotel SET hotel_name = ?, address = ? WHERE id = ?"
	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}	

	defer stmt.Close()
	_, err = stmt.ExecContext(ctx, req.HotelName, req.Address, req.Id)
	
	return err
}

func (repo *hotelRepoImpl) GetHotelById(id int64)(res model.HotelUpdate, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, hotel_name, address FROM hotel WHERE id= ?"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id)
	err = row.Scan(&res.Id, &res.HotelName, &res.Address)
	
	return res, err
}
