package repository

import (
	"bobobox_assignment/config"
	"bobobox_assignment/model"
	"database/sql"
)

type promotionRepoImpl struct {
	sqlDb 		*sql.DB
}

func NewPromotionRepo(database *sql.DB) PromotionRepo {
	return &promotionRepoImpl{
		sqlDb: 		database,
	}
}

func (repo *promotionRepoImpl) Create(req model.PromotionAdd)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "INSERT INTO promotion(promo_code, value, type, quota, date_start, date_end, status, minimum_nights, minimium_room, checkin_start, checkin_end, booking_day_start, booking_day_end, booking_at_start, booking_at_end, promo_used) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}	
	
	defer stmt.Close()

	var Status int
	if !req.Status {
		Status = 0
	}else {
		Status = 1
	}

	_, err = stmt.ExecContext(ctx, req.PromoCode, req.Value, req.Type, req.Quota, req.DateStart, req.DateEnd, Status, req.MinimumNights, req.MinimiumRoom, config.NewNullString(req.CheckinStart), config.NewNullString(req.CheckinEnd), config.NewNullString(req.BookingDayStart), config.NewNullString(req.BookingDayEnd), config.NewNullString(req.BookingAtStart), config.NewNullString(req.BookingAtEnd), req.PromoUsed)
	
	return err
}

func (repo *promotionRepoImpl) Update(req model.Promotion)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE promotion SET promo_code = ?, value = ?, type = ?, quota = ?, date_start = ?, date_end = ?, status = ?, minimum_nights = ?, minimium_room = ?, checkin_start = ?, checkin_end = ?, booking_day_start = ?, booking_day_end = ?, booking_at_start = ?, booking_at_end = ?, promo_used WHERE id = ?"

	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}	

	defer stmt.Close()
	var Status int
	if !req.Status {
		Status = 0
	}else {
		Status = 1
	}

	_, err = stmt.ExecContext(ctx, req.PromoCode, req.Value, req.Type, req.Quota, req.DateStart, req.DateEnd, Status, req.MinimumNights, req.MinimiumRoom, config.NewNullString(req.CheckinStart), config.NewNullString(req.CheckinEnd), config.NewNullString(req.BookingDayStart), config.NewNullString(req.BookingDayEnd), config.NewNullString(req.BookingAtStart), config.NewNullString(req.BookingAtEnd), req.PromoUsed, req.Id)
	
	return err
}

func (repo *promotionRepoImpl) UpdateStatus(req model.PromotionUpdateStatus)(err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	query := "UPDATE promotion SET status = ? WHERE id = ?"

	stmt, err := repo.sqlDb.PrepareContext(ctx, query)
	if err != nil {
		return err
	}	

	defer stmt.Close()
	var Status int
	if !req.Status {
		Status = 0
	}else {
		Status = 1
	}
	_, err = stmt.ExecContext(ctx, Status, req.Id)
	return err
}

func (repo *promotionRepoImpl) GetPromotionById(id int64)(res model.Promotion, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, promo_code, value, type, quota, date_start, date_end, status, minimum_nights, minimium_room, checkin_start, checkin_end, booking_day_start, booking_day_end, booking_at_start, booking_at_end, promo_used FROM promotion WHERE id= ?"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id)
	var Status int
	err = row.Scan(&res.Id, &res.PromoCode, &res.Value, &res.Type, &res.Quota, &res.DateStart, &res.DateEnd, &Status, &res.MinimumNights, &res.MinimiumRoom, &res.CheckinStart, &res.CheckinEnd, &res.BookingDayStart, &res.BookingDayEnd, &res.BookingAtStart, &res.BookingAtEnd, &res.PromoUsed)
	if Status == 0 {
		res.Status = false
	}else {
		res.Status = true
	}
	return res, err
}

func (repo *promotionRepoImpl) GetPromotionActive(id int64, dateNow string)(res model.Promotion, err error){
	ctx, cancel := config.NewMySqlContext()
	defer cancel()

	sqlStatement := "SELECT id, promo_code, value, type, quota, date_start, date_end, status, minimum_nights, minimium_room, checkin_start, checkin_end, booking_day_start, booking_day_end, booking_at_start, booking_at_end, promo_used FROM promotion WHERE id= ? AND status = 1 AND date_start <= ? AND date_end >= ? AND quota > promo_used"
	row := repo.sqlDb.QueryRowContext(ctx, sqlStatement, id, dateNow, dateNow)

	err = row.Scan(&res.Id, &res.PromoCode, &res.Value, &res.Type, &res.Quota, &res.DateStart, &res.DateEnd, &res.Status, &res.MinimumNights, &res.MinimiumRoom, &res.CheckinStart, &res.CheckinEnd, &res.BookingDayStart, &res.BookingDayEnd, &res.BookingAtStart, &res.BookingAtEnd, &res.PromoUsed)

	return res, err
}
