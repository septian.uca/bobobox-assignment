package repository

import "bobobox_assignment/model"

type HotelRepo interface {
	Create(req model.Hotel)(err error)
	Update(req model.HotelUpdate)(err error)
	GetHotelById(id int64)(res model.HotelUpdate, err error)
}