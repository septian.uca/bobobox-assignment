package repository

import "bobobox_assignment/model"

type PromotionRepo interface {
	Create(req model.PromotionAdd)(err error)
	Update(req model.Promotion)(err error)
	UpdateStatus(req model.PromotionUpdateStatus)(err error)
	GetPromotionById(id int64)(res model.Promotion, err error)
	GetPromotionActive(id int64, dateNow string)(res model.Promotion, err error)
}