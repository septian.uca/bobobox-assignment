package controller

import (
	"bobobox_assignment/exception"
	"bobobox_assignment/helper"
	"bobobox_assignment/model"
	"bobobox_assignment/usecase"

	"github.com/gofiber/fiber/v2"
)

type PromotionController struct {
	PromotionUsecase usecase.PromotionUsecase
}

func NewPromotionController(PromotionUsecase *usecase.PromotionUsecase) PromotionController {
	return PromotionController{
		PromotionUsecase: 		*PromotionUsecase,
	}
}

func (controller *PromotionController) Route(app *fiber.App) {
	app.Post("/api/promotion", controller.Create)
	app.Put("/api/promotion", controller.Update)
	app.Patch("/api/promotion", controller.UpdateStatus)
	app.Post("/api/promotion/getpromo", controller.GetPromotionRoom)
}

func (controller *PromotionController) Create(c *fiber.Ctx) error {
	var request model.PromotionAdd
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	controller.PromotionUsecase.Create(request)
	message := "Create promotion successfull"
	return helper.ResponseOk(c,nil,message)
}

func (controller *PromotionController) Update(c *fiber.Ctx) error {
	var request model.Promotion
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)
	
	controller.PromotionUsecase.Update(request)
	message := "Update promotion successfull"
	return helper.ResponseOk(c,nil,message)
}

func (controller *PromotionController) UpdateStatus(c *fiber.Ctx) error {
	var request model.PromotionUpdateStatus
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)
	
	controller.PromotionUsecase.UpdateStatus(request)
	message := "Update status promotion successfull"
	return helper.ResponseOk(c,nil,message)
}

func (controller *PromotionController) GetPromotionRoom(c *fiber.Ctx) error {
	var request model.GetPromotionRoomList
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	data, message := controller.PromotionUsecase.GetPromotion(request)
	return helper.ResponseOk(c, data, message)
}