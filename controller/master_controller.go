package controller

import (
	"bobobox_assignment/exception"
	"bobobox_assignment/helper"
	"bobobox_assignment/model"
	"bobobox_assignment/usecase"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type MasterController struct {
	MasterUsecase		usecase.MasterUsecase
}

func NewMasterController(MasterUsecase *usecase.MasterUsecase) MasterController {
	return MasterController{
		MasterUsecase: *MasterUsecase,
	}
}

func (controller *MasterController) Route(app *fiber.App) {
	app.Get("/api/availability", controller.Availability)
	app.Post("/api/room", controller.CreateRoom)
	app.Put("/api/room", controller.UpdateRoom)
	app.Post("/api/hotel", controller.CreateHotel)
	app.Put("/api/hotel", controller.UpdateHotel)
	app.Post("/api/room-price", controller.CreateRoomPrice)
	app.Put("/api/room-price", controller.UpdateRoomPrice)
	app.Post("/api/room-type", controller.CreateRoomType)
	app.Put("/api/room-type", controller.UpdateRoomType)
	// app.Post("/api/order", controller.Order)
}

func (controller *MasterController) CreateRoom(c *fiber.Ctx) error {
	var request model.Room 
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.CreateRoom(request)
	message := "create room successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) UpdateRoom(c *fiber.Ctx) error {
	var request model.RoomUpdate
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.UpdateRoom(request)
	message := "update room successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) CreateHotel(c *fiber.Ctx) error {
	var request model.Hotel
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.CreateHotel(request)
	message := "create hotel successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) UpdateHotel(c *fiber.Ctx) error {
	var request model.HotelUpdate
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.UpdateHotel(request)
	message := "update hotel successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) CreateRoomPrice(c *fiber.Ctx) error {
	var request model.PriceCreate
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	message := "create room price successfull"
	err = controller.MasterUsecase.CreateRoomPrice(request)
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) UpdateRoomPrice(c *fiber.Ctx) error {
	var request model.Price
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.UpdateRoomPrice(request)
	message := "update room price successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) CreateRoomType(c *fiber.Ctx) error {
	var request model.CreateRoomType
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.CreateRoomType(request.Name)
	message := "create room type successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

func (controller *MasterController) UpdateRoomType(c *fiber.Ctx) error {
	var request model.RoomType
	err := c.BodyParser(&request)
	exception.PanicBadRequest(err)

	err = controller.MasterUsecase.UpdateRoomType(request)
	message := "update room type successfull"
	if err != nil {
		message = err.Error()
	}
	return helper.ResponseOk(c,nil,message)
}

// func (controller *MasterController) Order(c *fiber.Ctx) error {
// 	var request model.OrderRequest
// 	err := c.BodyParser(&request)
// 	exception.PanicBadRequest(err)

// 	err = controller.Order
// }

func (controller *MasterController) Availability(c *fiber.Ctx) error {
	var request model.AvailabilityReq

	request.CheckinDate = c.Query("checkin_date")
	request.CheckoutDate = c.Query("checkout_date")

	RoomQty, err := strconv.Atoi(c.Query("room_qty"))
	if err != nil {
		RoomQty = 1
	}

	RoomTypeId, err := strconv.Atoi(c.Query("room_type_id"))
	if err != nil {
		RoomTypeId = 1
	}

	request.RoomQty = int8(RoomQty)
	request.RoomTypeId = int8(RoomTypeId)

	data, err := controller.MasterUsecase.GetRoomAvailability(request)
	if err != nil{
		return helper.InternalServerError(c, err, 500)
	}

	message := "Room Availability"
	return helper.ResponseOk(c, data, message)
}