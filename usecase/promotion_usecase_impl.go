package usecase

import (
	"bobobox_assignment/helper"
	"bobobox_assignment/model"
	"bobobox_assignment/repository"
	"bobobox_assignment/validation"
	"strconv"
	"time"
)

type promotionUsecaseImpl struct {
	PromotionRepo 	repository.PromotionRepo
}

func NewPromotionUsecasce(promotionRepo *repository.PromotionRepo) PromotionUsecase {
	return &promotionUsecaseImpl {
		PromotionRepo: *promotionRepo,
	}
}

func (usecase *promotionUsecaseImpl) Create(req model.PromotionAdd)(err error){
	validation.ValidationCreate(req)

	err = usecase.PromotionRepo.Create(req)
	return err
}

func (usecase *promotionUsecaseImpl) Update(req model.Promotion)(err error){
	validation.ValidationUpdate(req)
	
	err = usecase.PromotionRepo.Update(req)
	return err
}

func (usecase *promotionUsecaseImpl) UpdateStatus(req model.PromotionUpdateStatus)(err error){
	validation.ValidationUpdateStatus(req)
	usecase.PromotionRepo.UpdateStatus(req)	
	return err
}

func (usecase *promotionUsecaseImpl) GetPromotion(req model.GetPromotionRoomList)(res []model.PromotionList, message string){
	dateNow := helper.DateNowString()
	promo, err := usecase.PromotionRepo.GetPromotionActive(req.PromoId, dateNow)
	if err != nil {
		return nil, err.Error()
	}

	numOfRooms := len(req.Rooms)
	if numOfRooms < int(promo.MinimiumRoom){
		strRoom := strconv.Itoa(int(promo.MinimiumRoom))
		return nil, "Minimum number of room reservations is " + strRoom + " rooms"
	}
	var arrDate []string
	var totalPrice float64
	var dateInt int64
	for _, room := range req.Rooms {
		for _, price := range room.Price {
			totalPrice += price.Price
			t := helper.DateStringToInt(price.Date)
			if dateInt == 0 || dateInt > t {
				dateInt = t
			}

			_, found := helper.IsExistInArray(arrDate, price.Date)
			if !found {
				arrDate = append(arrDate, price.Date)
			}
		}
	}

	//checking eligible promotion
	if len(arrDate) < int(promo.MinimumNights){
		return nil, "minimum number of stays is "+strconv.Itoa(int(promo.MinimumNights))+" nights"
	}

	if len(promo.CheckinStart) > 0 && len(promo.CheckinEnd) > 0 {
		checkInIntS := (helper.ParseDateFormat(promo.CheckinStart)).UnixNano()/int64(time.Millisecond)
		checkInIntE := (helper.ParseDateFormat(promo.CheckinEnd)).UnixNano()/int64(time.Millisecond)
		if dateInt < checkInIntS || dateInt > checkInIntE {
			return nil, "Promotion can use if check-In between " +promo.CheckinStart+ " to " + promo.CheckinEnd
		}
	}
	currentTimeInt := helper.DateNowInt()

	bookingDateStartInt := (helper.ParseDateFormat(promo.BookingDayStart)).UnixNano()/int64(time.Millisecond)
	bookingDate := helper.ParseDateFormat(promo.BookingDayEnd)
	bookingDateEnd := helper.AddLastTimeInDay(bookingDate)
	bookingDateEndInt := bookingDateEnd.UnixNano()/int64(time.Millisecond)
	if currentTimeInt < bookingDateStartInt || currentTimeInt > bookingDateEndInt {
		return nil, "Promotion can use if booking between " +promo.BookingDayStart+ " to " + promo.BookingDayEnd
	}

	startAt := (helper.ParseDateFormat(promo.BookingAtStart)).UnixNano() /int64(time.Millisecond)
	endAt := (helper.ParseDateFormat(promo.BookingAtEnd)).UnixNano() /int64(time.Millisecond)
	if currentTimeInt < startAt || currentTimeInt > endAt {
		return nil, "Promotion can use if booking betweent " +promo.BookingAtStart+ " to " + promo.BookingAtEnd
	}

	var totalPromo float64
	if promo.Type == "flat" {
		totalPromo = promo.Value
	} else {
		totalPromo = totalPrice * promo.Value/100
	}

	res = append(res, model.PromotionList{
		PromoCode: 				promo.PromoCode,
		Value: 					promo.Value,
		Type: 					promo.Type,
		TotalPromo: 			totalPromo,
		TotalAfterPromo:		totalPrice - totalPromo,
		RequestPromo: 			req,	
	});

	return res, "Get Promotion successfull"
}
