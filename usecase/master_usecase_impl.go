package usecase

import (
	"bobobox_assignment/helper"
	"bobobox_assignment/model"
	"bobobox_assignment/repository"
	"bobobox_assignment/validation"
	"errors"

	"github.com/jinzhu/copier"
)

type masterUsecaseImpl struct {
	RoomsRepo			repository.RoomsRepo
	HotelRepo			repository.HotelRepo
}

func NewMasterUsecase(RoomsRepo *repository.RoomsRepo, HotelRepo *repository.HotelRepo) MasterUsecase {
	return &masterUsecaseImpl {
		RoomsRepo: 		*RoomsRepo,
		HotelRepo: 		*HotelRepo,
	}
}

func (usecase *masterUsecaseImpl) GetRoomAvailability(req model.AvailabilityReq)(res []model.AvailabilityRes, err error){

	data, err :=  usecase.RoomsRepo.GetRoomAvailability(req)
	if err != nil {
		return nil, err
	}

	var AvailabelRoom []model.AvailableRoom
	var minPrice float64
	for indx, obj := range data {
		i, found := helper.FindRoom(AvailabelRoom, obj.RoomId)
		if indx == 0 || minPrice > obj.Price{
			minPrice = obj.Price
		}

		if !found {
			var Prices []model.Prices
			Prices = append(Prices, model.Prices{
				Date: 			obj.Date,
				Price: 			obj.Price,
			})
			AvailabelRoom = append(AvailabelRoom, model.AvailableRoom{
				RoomId: 		obj.RoomId,
				RoomNumber: 	obj.RoomNumber,
				Price: 			Prices,
			})
		} else {
			AvailabelRoom[i].Price = append(AvailabelRoom[i].Price, model.Prices{
				Date: 			obj.Date,
				Price: 			obj.Price,
			})
		}
	}
	days := helper.GetDays(req.CheckinDate, req.CheckoutDate)
	res = append(res, model.AvailabilityRes{
		AvailabilityReq: 	req,
		TotalPrice: 		minPrice * float64(req.RoomQty) * days,
		AvailableRoom: 		AvailabelRoom,
	})
	return res, err
}

func (usecase *masterUsecaseImpl) CreateHotel(req model.Hotel)(err error){
	validation.ValidationInputHotel(req)
	
	err = usecase.HotelRepo.Create(req)
	return err
}

func (usecase *masterUsecaseImpl) CreateRoom(req model.Room)(err error){
	validation.ValidationInputRoom(req)

	err = usecase.RoomsRepo.CreateRoom(req)
	return err
}

func (usecase *masterUsecaseImpl) CreateRoomType(name string)(err error){
	if len(name) < 5{
		return errors.New("input at least 5 length room type name")
	}

	err = usecase.RoomsRepo.CreateRoomType(name)
	return err
}

func (usecase *masterUsecaseImpl) CreateRoomPrice(req model.PriceCreate)(err error){
	validation.ValidationInputPriceRoom(req)

	err = usecase.RoomsRepo.CreateRoomPrice(req)
	return err
}

func (usecase *masterUsecaseImpl) UpdateHotel(req model.HotelUpdate)(err error){
	var request model.Hotel
	copier.Copy(&request, req)

	validation.ValidationInputHotel(request)
	validation.ValidationIdHotel(req)

	err = usecase.HotelRepo.Update(req)
	return err
}

func (usecase *masterUsecaseImpl) UpdateRoom(req model.RoomUpdate)(err error){
	var request model.Room
	copier.Copy(&request, req)
	validation.ValidationInputRoom(request)
	validation.ValidationIdRoom(req)

	err = usecase.RoomsRepo.UpdateRoom(req)
	return err
}

func (usecase *masterUsecaseImpl) UpdateRoomType(req model.RoomType)(err error){
	validation.ValidationUpdataRoomtype(req)

	err = usecase.RoomsRepo.UpdateRoomType(req)
	return err
}

func (usecase *masterUsecaseImpl) UpdateRoomPrice(req model.Price)(err error){
	var request model.PriceCreate
	copier.Copy(&request, req)

	validation.ValidationIdPrice(req)
	validation.ValidationInputPriceRoom(request)

	err = usecase.RoomsRepo.UpdateRoomPrice(req)
	return err
}
