package usecase

import "bobobox_assignment/model"

type PromotionUsecase interface {
	Create(req model.PromotionAdd)(err error)
	Update(req model.Promotion)(err error)
	UpdateStatus(req model.PromotionUpdateStatus)(err error)
	GetPromotion(req model.GetPromotionRoomList)(res []model.PromotionList, message string)
}