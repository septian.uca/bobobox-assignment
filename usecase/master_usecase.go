package usecase

import "bobobox_assignment/model"

type MasterUsecase interface {
	GetRoomAvailability(req model.AvailabilityReq)(res []model.AvailabilityRes, err error)
	CreateHotel(req model.Hotel)(err error)
	CreateRoom(req model.Room)(err error)
	CreateRoomType(name string)(err error)
	CreateRoomPrice(req model.PriceCreate)(err error)
	UpdateHotel(req model.HotelUpdate)(err error)
	UpdateRoom(req model.RoomUpdate)(err error)
	UpdateRoomType(req model.RoomType)(err error)
	UpdateRoomPrice(req model.Price)(err error)
}