-- bobobox_asignment.hotel definition

CREATE TABLE `hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- bobobox_asignment.promotion definition

CREATE TABLE `promotion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(20) NOT NULL,
  `value` decimal(10,0) NOT NULL,
  `type` varchar(12) NOT NULL COMMENT '(`percentage`, `flat`)',
  `quota` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_nights` int(11) NOT NULL DEFAULT '1',
  `minimium_room` int(11) NOT NULL DEFAULT '1',
  `checkin_start` datetime DEFAULT NULL,
  `checkin_end` datetime DEFAULT NULL,
  `booking_day_start` datetime DEFAULT NULL,
  `booking_day_end` datetime DEFAULT NULL,
  `booking_at_start` datetime DEFAULT NULL,
  `booking_at_end` datetime DEFAULT NULL,
  `promo_used` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- bobobox_asignment.reservation definition

CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(15) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `booked_room_count` int(11) NOT NULL,
  `checkin_date` datetime NOT NULL,
  `checout_date` datetime NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`,`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- bobobox_asignment.room definition

CREATE TABLE `room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) NOT NULL,
  `room_type_id` bigint(20) NOT NULL,
  `room_number` varchar(20) NOT NULL,
  `room_status` varchar(14) NOT NULL COMMENT '(‘available’, ‘out of service’)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;


-- bobobox_asignment.room_type definition

CREATE TABLE `room_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


-- bobobox_asignment.stay definition

CREATE TABLE `stay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reservation_id` bigint(20) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `room_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- bobobox_asignment.stay_room definition

CREATE TABLE `stay_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stay_id` bigint(20) NOT NULL,
  `room_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


-- bobobox_asignment.price definition

CREATE TABLE `price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `room_type_id` bigint(20) NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `room_type_id` (`room_type_id`),
  CONSTRAINT `price_ibfk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;