INSERT INTO bobobox_asignment.hotel
(id, hotel_name, address)
VALUES(1, 'Bobobox Jakarta', 'Kebayotan Lama');
INSERT INTO bobobox_asignment.hotel
(id, hotel_name, address)
VALUES(2, 'Bobobox Bandung', 'Lembang');
INSERT INTO bobobox_asignment.hotel
(id, hotel_name, address)
VALUES(3, 'Bobobox Solo', 'Solo');
INSERT INTO bobobox_asignment.hotel
(id, hotel_name, address)
VALUES(4, 'Bobobox Malang', 'Malang');


INSERT INTO bobobox_asignment.price
(id, `date`, room_type_id, price)
VALUES(1, '2021-11-21', 1, 150000.0);
INSERT INTO bobobox_asignment.price
(id, `date`, room_type_id, price)
VALUES(2, '2021-11-21', 2, 200000.0);
INSERT INTO bobobox_asignment.price
(id, `date`, room_type_id, price)
VALUES(3, '2021-11-22', 1, 110000.0);
INSERT INTO bobobox_asignment.price
(id, `date`, room_type_id, price)
VALUES(4, '2021-11-22', 2, 130000.0);


INSERT INTO bobobox_asignment.promotion
(id, promo_code, value, `type`, quota, date_start, date_end, status, minimum_nights, minimium_room, checkin_start, checkin_end, booking_day_start, booking_day_end, booking_at_start, booking_at_end, promo_used)
VALUES(1, 'TES09', 50, 'percentage', 100, '2021-11-23', '2021-11-25', 1, 1, 1, '2021-11-23 00:00:00', '2021-11-23 00:00:00', '2021-11-23 00:00:00', '2021-11-23 23:59:59', '2021-11-23 10:00:00', '2021-11-24 11:00:11', 0);


INSERT INTO bobobox_asignment.reservation
(id, order_id, customer_name, booked_room_count, checkin_date, checout_date, hotel_id)
VALUES(1, 'A300029392', 'Maman Surahman', 1, '2021-11-21 00:00:00', '2021-11-22 00:00:00', 1);

INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(1, 1, 1, '100', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(2, 1, 1, '102', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(3, 2, 1, '102', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(4, 2, 1, '100', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(5, 3, 1, '102', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(6, 3, 1, '100', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(7, 3, 1, '103', 'out of service');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(8, 4, 2, '99', 'available');
INSERT INTO bobobox_asignment.room
(id, hotel_id, room_type_id, room_number, room_status)
VALUES(9, 4, 1, '99', 'available');

INSERT INTO bobobox_asignment.room_type
(id, name)
VALUES(1, 'Single Room');
INSERT INTO bobobox_asignment.room_type
(id, name)
VALUES(2, 'Single Room');
INSERT INTO bobobox_asignment.room_type
(id, name)
VALUES(3, 'Double Room');
INSERT INTO bobobox_asignment.room_type
(id, name)
VALUES(4, 'Triple Room');


INSERT INTO bobobox_asignment.stay
(id, reservation_id, guest_name, room_id)
VALUES(1, 1, 'Maman Surahman', 1);

INSERT INTO bobobox_asignment.stay_room
(id, stay_id, room_id, `date`)
VALUES(1, 1, 1, '2021-11-21');
