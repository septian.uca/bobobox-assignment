package helper

import (
	"bobobox_assignment/model"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/araddon/dateparse"
)

func FindRoom(slice []model.AvailableRoom, val int64) (int, bool) {
    for i, item := range slice {
        if item.RoomId == val {
            return i, true
        }
    }
    return -1, false
}

func IsExistInArray(slice []string, val string) (int, bool) {
	for i, item := range slice {
        if item == val {
            return i, true
        }
    }
    return -1, false
}

func GetDays(firstDate, LastDate string)(float64){
	d1 := strings.Split(firstDate, "-")
	d2 := strings.Split(LastDate, "-")

	date1 := DayDate(d1[0], d1[1], d1[2])
    date2 := DayDate(d2[0], d2[1], d2[2])
    
	return date2.Sub(date1).Hours() / 24

}

func DayDate(year, month, day string) time.Time {
	yearInt, err := strconv.Atoi(year)
	if err != nil {
		log.Fatal(err)
	}

	monthInt, err := strconv.Atoi(month)
	if err != nil {
		log.Fatal(err)
	}
	
	dayInt, err := strconv.Atoi(day)
	if err != nil {
		log.Fatal(err)
	}
	
    return time.Date(yearInt, time.Month(monthInt), dayInt, 0, 0, 0, 0, time.UTC)
}

func DateNowString() string {
	currentTime := time.Now()
	return currentTime.Format("2006-01-02");
}

func DateNowInt() int64 {
	local, _ := time.LoadLocation("Asia/Jakarta")
	dateNow := time.Now().In(local)
	return dateNow.UnixNano() /int64(time.Millisecond);
}

func DateStringToInt(date string)(dateInt int64){
	t, _ := time.Parse("2006-01-02", date)
	return t.UnixNano()/int64(time.Millisecond)
}

func DateStringToDate(date string)(t time.Time){
	t, _ = time.Parse("2006-01-02", date)
	return t
}

func AddLastTimeInDay(Day time.Time) time.Time {
	addTwentyThreeHour := Day.Add(23 * time.Hour)
	addFiftyFiveMinute := addTwentyThreeHour.Add(59 * time.Minute)
	addFiftyFiveSeconds := addFiftyFiveMinute.Add(59 * time.Second)

	return addFiftyFiveSeconds
}

func ParseDateFormat(dateString string) time.Time{
	var timezone = "Asia/Jakarta"
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		panic(err.Error())
	}
	time.Local = loc
	t, err := dateparse.ParseLocal(dateString)
	if err != nil {
		panic(err.Error())
	}
	return t
}