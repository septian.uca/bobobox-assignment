package helper

import (
	"bobobox_assignment/model"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func ResponseOk(c *fiber.Ctx, data interface{}, message string)error{
	return c.Status(http.StatusOK).JSON(model.WebResponse{
		Message: message,
		Data:   data,
	})
}

func MethodNotAllowed(c *fiber.Ctx,err error,code int)error{
	return c.Status(http.StatusMethodNotAllowed).JSON(model.WebResponseError{
		Error: model.ErrorResponse{
			Message: err.Error(),
			Code:   code,
		},
	})
}

func BadRequest(c *fiber.Ctx,err error,code int)error{
	return c.Status(http.StatusBadRequest).JSON(model.WebResponseError{
		Error: model.ErrorResponse{
			Message: err.Error(),
			Code:   code,
		},
	})
}

func InternalServerError(c *fiber.Ctx,err error,code int)error{
	return c.Status(http.StatusInternalServerError).JSON(model.WebResponseError{
		Error: model.ErrorResponse{
			Message: err.Error(),
			Code:   code,
		},
	})
}