package exception

import (
	"bobobox_assignment/helper"

	"github.com/gofiber/fiber/v2"
)


type ValidationError struct {
	Message   string
	Status    int
	ErrorCode int
}

func (validationError ValidationError) Error() string {
	return validationError.Message
}

func PanicIfErr(err interface{}) {
	if err != nil {
		panic(err)
	}
}

func ErrorHandler(ctx *fiber.Ctx, err error) error {
	status, _ := err.(ValidationError)

	if status.Status==400 {
		return helper.BadRequest(ctx,err,status.ErrorCode)
	}

	if e, ok := err.(*fiber.Error); ok {
		if e.Code == 405 {
			return helper.MethodNotAllowed(ctx,err,3999)
		}
	}

	return helper.InternalServerError(ctx,err,status.ErrorCode)
}

func PanicBadRequest(err interface{}) {
	if err != nil {
		panic(ValidationError{
			Status:    400,
			ErrorCode: 3006,
			Message:   "Invalid request",
		})
	}
}