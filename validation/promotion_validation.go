package validation

import (
	"bobobox_assignment/config"
	"bobobox_assignment/exception"
	"bobobox_assignment/model"
	"bobobox_assignment/repository"
	"errors"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
)

func GetPromotion()(repo repository.PromotionRepo){
	SqlDb := config.NewMySqlDatabase()
	return repository.NewPromotionRepo(SqlDb)
}

func checkValidPromotion(value interface{}) error{
	repo := GetPromotion()
	id, _ := value.(int64)
	_, err := repo.GetPromotionById(id)
	if err != nil {
		return errors.New("invalid promotion id")
	}
	return nil
}

func ValidationCreate(request model.PromotionAdd){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Value, validation.Required),
		validation.Field(&request.Type, validation.Required),
		validation.Field(&request.Status, validation.In(true,false)),
		validation.Field(&request.Quota, validation.Required),
		validation.Field(&request.PromoCode, validation.Required),
		validation.Field(&request.MinimumNights, validation.Required),
		validation.Field(&request.MinimiumRoom, validation.Required),
		validation.Field(&request.DateStart, validation.Required),
		validation.Field(&request.DateEnd, validation.Required),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationUpdate(request model.Promotion){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.Required, validation.By(checkValidPromotion)),
		validation.Field(&request.Value, validation.Required),
		validation.Field(&request.Type, validation.Required),
		validation.Field(&request.Status, validation.In(true,false)),
		validation.Field(&request.Quota, validation.Required),
		validation.Field(&request.PromoCode, validation.Required),
		validation.Field(&request.MinimumNights, validation.Required),
		validation.Field(&request.MinimiumRoom, validation.Required),
		validation.Field(&request.DateStart, validation.Required),
		validation.Field(&request.DateEnd, validation.Required),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationUpdateStatus(request model.PromotionUpdateStatus){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.Required, validation.By(checkValidPromotion)),
		validation.Field(&request.Status, validation.In(true,false)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}