package validation

import (
	"bobobox_assignment/config"
	"bobobox_assignment/exception"
	"bobobox_assignment/model"
	"bobobox_assignment/repository"
	"errors"
	"fmt"
	"net/http"

	validation "github.com/go-ozzo/ozzo-validation"
)

func GetHotel()(repo repository.HotelRepo){
	SqlDb := config.NewMySqlDatabase()
	return repository.NewHotelRepo(SqlDb)
}

func GetRooms()(repo repository.RoomsRepo){
	SqlDb := config.NewMySqlDatabase()
	return repository.NewRoomsRepo(SqlDb)
}

func checkValidHotel(value interface{}) error{
	
	repo := GetHotel()
	id, _ := value.(int64)
	_, err := repo.GetHotelById(id)
	if err != nil {
		return errors.New("invalid hotel id")
	}
	return nil
}

func checkValidRoomType(value interface{}) error {
	repo := GetRooms()
	id, _ := value.(int8)
	fmt.Println(id)
	_, err := repo.RoomTypeById(id)
	if err != nil {
		return errors.New("invalid room type id")
	}
	return nil
}

func checkValidRoomPrice(value interface{}) error {
	repo := GetRooms()
	id, _ := value.(int64)
	_, err := repo.RoomPriceById(id)
	if err != nil {
		return errors.New("invalid room price id")
	}
	return nil
}

func checkIdRoom(value interface{}) error {
	repo := GetRooms()
	id, _ := value.(int64)
	_, err := repo.RoomById(id)
	if err != nil {
		return errors.New("invalid room id")
	}
	return nil
}

func ValidationAvailabilityReq(request model.AvailabilityReq){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.CheckinDate, validation.Required),
		validation.Field(&request.CheckoutDate, validation.Required),
		validation.Field(&request.RoomQty, validation.Required),
		validation.Field(&request.RoomTypeId, validation.Required),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationInputHotel(request model.Hotel){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Address, validation.Required),
		validation.Field(&request.HotelName, validation.Required),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationIdHotel(request model.HotelUpdate){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.By(checkValidHotel)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationInputRoom(request model.Room){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.RoomNumber, validation.Required),
		validation.Field(&request.RoomStatus, validation.In("available","out of service")),
		validation.Field(&request.RoomTypeId, validation.By(checkValidRoomType)),
		validation.Field(&request.HotelId, validation.By(checkValidHotel)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationIdRoom(request model.RoomUpdate){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.By(checkIdRoom)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationUpdataRoomtype(request model.RoomType){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.By(checkValidRoomType)),
		validation.Field(&request.Name, validation.Length(5, 100)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationInputPriceRoom(request model.PriceCreate){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.RoomTypeId, validation.By(checkValidRoomType)),
		validation.Field(&request.Price, validation.Required),
		validation.Field(&request.Date, validation.Required),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}

func ValidationIdPrice(request model.Price){
	err := validation.ValidateStruct(&request,
		validation.Field(&request.Id, validation.By(checkValidRoomPrice)),
	)
	if err != nil {
		panic(exception.ValidationError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			ErrorCode: 400,
		})
	}
}