package main

import (
	"bobobox_assignment/config"
	"bobobox_assignment/controller"
	"bobobox_assignment/exception"
	"bobobox_assignment/repository"
	"bobobox_assignment/usecase"

	"github.com/gofiber/fiber/v2"
)


func main(){
	sqlDb := config.NewMySqlDatabase()
    app := fiber.New()

	roomRepo := repository.NewRoomsRepo(sqlDb)
	hotelRepo := repository.NewHotelRepo(sqlDb)
	masterUsecase := usecase.NewMasterUsecase(&roomRepo, &hotelRepo)
	masterController := controller.NewMasterController(&masterUsecase)

	promotionRepo := repository.NewPromotionRepo(sqlDb)
	promotionUsecase := usecase.NewPromotionUsecasce(&promotionRepo)
	promotionController := controller.NewPromotionController(&promotionUsecase)

	masterController.Route(app)
	promotionController.Route(app)
	
	err := app.Listen(":3000")
	exception.PanicIfErr(err)
}