CREATE DATABASE hotel_db;

CREATE TABLE `hotel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `room_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hotel_id` bigint(20) NOT NULL,
  `room_type_id` bigint(20) NOT NULL,
  `room_number` varchar(20) NOT NULL,
  `room_status` varchar(14) NOT NULL COMMENT '(‘available’, ‘out of service’)',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`hotel_id`) REFERENCES `hotel`(id),
  FOREIGN KEY (`room_type_id`) REFERENCES `room_type`(id),
);


CREATE TABLE `room_price` (
  `id` bigint(20) NOT NULL,
  `room_type_id` bigint(20) NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`date`),
  FOREIGN KEY (`room_type_id`) REFERENCES `room_type`(id),
  FOREIGN KEY (`hotel_id`) REFERENCES `hotel`(id)
);


-- bobobox_asignment.reservation definition

CREATE TABLE `reservation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(15) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `booked_room_count` int(11) NOT NULL,
  `checkin_date` datetime NOT NULL,
  `checout_date` datetime NOT NULL,
  `hotel_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`,`order_id`),
  FOREIGN KEY (`hotel_id`) REFERENCES `hotel`(id),
);


-- bobobox_asignment.stay definition

CREATE TABLE `stay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reservation_id` bigint(20) NOT NULL,
  `guest_name` varchar(100) NOT NULL,
  `room_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`room_id`) REFERENCES `room`(id),
);


-- bobobox_asignment.stay_room definition

CREATE TABLE `stay_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `stay_id` bigint(20) NOT NULL,
  `room_id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`room_id`) REFERENCES `room`(id)
);