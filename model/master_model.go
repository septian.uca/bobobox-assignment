package model

type Hotel struct {
	HotelName		string `json:"hotel_name"`
	Address 		string `json:"address"`
}

type HotelUpdate struct {
	Id				int64 `json:"id"`
	HotelName		string `json:"hotel_name"`
	Address 		string `json:"address"`
}

type Room struct {
	HotelId			int64 `json:"hotel_id"`
	RoomTypeId		int8 `json:"room_type_id"`
	RoomNumber 		int16 `json:"room_number"`
	RoomStatus		string `json:"room_status"`
}

type RoomUpdate struct {
	Id				int64 `json:"id"`
	HotelId			int64 `json:"hotel_id"`
	RoomTypeId		int8 `json:"room_type_id"`
	RoomNumber 		int16 `json:"room_number"`
	RoomStatus		string `json:"room_status"`
}

type RoomType struct {
	Id				int8 `json:"id"`
	Name			string `json:"name"`
}

type Price struct {
	Id				int64 `json:"id"`
	Date			string `json:"date"`
	RoomTypeId	int8 `json:"room_type_id"`
	Price			float64 `json:"price"`
}

type PriceCreate struct {
	Date			string `json:"date"`
	RoomTypeId	int8 `json:"room_type_id"`
	Price			float64 `json:"price"`
}

type AvailabilityReq struct {
	CheckinDate 		string `json:"checkin_date"`
	CheckoutDate 		string `json:"checkout_date"`
	RoomQty 			int8 `json:"room_qty"`
	RoomTypeId 			int8 `json:"room_type_id"`
}

type AvailabilityRes struct {
	AvailabilityReq		
	TotalPrice			float64 `json:"total_price"`
	AvailableRoom		[]AvailableRoom `json:"availableRoom,omitempty"`
}

type AvailableRoom struct {
	RoomId 				int64 `json:"room_id"`
	RoomNumber 			int16 `json:"room_number"`
	Price 				[]Prices `json:"price"`
}

type Prices struct {
	Date				string `json:"date"`
	Price				float64 `json:"price"`
}

type AvailableRoomRepo struct {
	RoomId 				int64 `json:"room_id"`
	RoomNumber 			int16 `json:"room_number"`
	Date				string `json:"date"`
	Price				float64 `json:"price"`
}

type CreateRoomType struct {
	Name			string `json:"name"`
}