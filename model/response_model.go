package model

type WebResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

type WebResponsePipeline struct {
	Code   int         `json:"code"`
	Status string      `json:"status"`
	Data   interface{} `json:"data,omitempty"`
}

type ErrorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type WebResponseError struct {
	Error ErrorResponse `json:"error"`
}

type ValidationError struct {
	Message   string
	Status    int
	ErrorCode int
}