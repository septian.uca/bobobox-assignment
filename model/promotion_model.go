package model

type Promotion struct {
	Id 						int64 `json:"id"`
	PromotionAdd
}

type PromotionAdd struct {
	PromoCode 				string `json:"promo_code"`
	Value 					float64 `json:"value"`
	Type 					string `json:"type"`
	Quota 					int8 `json:"quota"`
	DateStart 				string `json:"date_start"`
	DateEnd 				string `json:"date_end"`
	Status 					bool `json:"status"`
	MinimumNights 			int8 `json:"minimum_nights"`
	MinimiumRoom 			int8 `json:"minimium_room"`
	CheckinStart 			string `json:"checkin_start"`
	CheckinEnd 				string `json:"checkin_end"`
	BookingDayStart 		string `json:"booking_day_start"`
	BookingDayEnd 			string `json:"booking_day_end"`
	BookingAtStart 			string `json:"booking_at_start"`
	BookingAtEnd 			string `json:"booking_at_end"`
	PromoUsed 				int8 `json:"promo_used"`
}

type PromotionUpdateStatus struct {
	Id 						int64 `json:"id"`
	Status 					bool `json:"status"`
}

type GetPromotionRoomList struct {
	TotalPrice			float64 `json:"total_price"`
	PromoId				int64 `json:"promo_id"`
	Rooms 				[]Rooms `json:"rooms"`
}

type Rooms struct {
	RoomId 				int64 `json:"room_id"`
	Price 				[]Prices `json:"price"`
}

type PromotionList struct {
	PromoCode 				string `json:"promo_code"`
	Value 					float64 `json:"value"`
	Type 					string `json:"type"`
	TotalPromo				float64 `json:"total_promo"`
	TotalAfterPromo			float64 `json:"total_after_promo"`
	RequestPromo 			GetPromotionRoomList
}