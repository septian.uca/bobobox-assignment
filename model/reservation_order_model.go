package model

type OrderRequest struct {
	CustomerName		string `json:"customer_name"`
	CheckinDate			string `json:"checkin_date"`
	CheckoutDate		string `json:"checkout_date"`
	HotelId 			int64 `json:"hotel_id"`
	Rooms 				[]RoomOrder `json:"rooms"`
}

type RoomOrder struct {
	RoomId 			int64 `json:"room_id"`
	Date			string `json:"date"`
}