package model

type Reservation struct{
	Id					int64 `json:"id"`
	OrderId				string `json:"order_id"`
	CustomerName		string `json:"customer_name"`
	BookedRoomCount		int8 `json:"booked_room_count"`
	CheckinDate			string `json:"checkin_date"`
	ChecoutDate			string `json:"checout_date"`
	HotelId				int64 `json:"hotel_id"`
}

type Stay struct {
	Id					int64 `json:"id"`
	ReservationId		int64 `json:"reservation_id"`
	GuestName			string `json:"guest_name"`
	RoomId				int64 `json:"room_id"`
}

type StayRoom struct {
	Id 					int64 `json:"id"`
	StayId 				int64 `json:"stay_id"`
	RoomId 				int64 `json:"room_id"`
	Date 				string `json:"date"`
}