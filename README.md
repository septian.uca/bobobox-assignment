# Bobobox Assignment

**How To Initiate Project :**
1. Pull project ke local anda
2. Kemudian pada command line masuk ke directory project golang anda
3. Ketikan `go init <directory project>`
4. Secara otomatis go.mod fie akan diperbaharui menyesuaikan environment golang di local anda
5. Setelah itu ketikan `go mod tidy`, Perintah ini akan mengunduh semua dependensi yang diperlukan dalam file source Anda dan memperbarui file go.mod dengan dependensi tersebut. 
6. untuk menjalankan aplikasi di local cukup menjalankan code `go run main.go`

**Apa Saja yang ada dalam Project ini :**

Project ini menggunakan GoFiber (lihat [](https://gofiber.io/)) sebagai framework untuk memudahkan proses development.

Project ini menggunakan paradigma clean-architecture, dalam hal ini saya menggunakan beberapa layer, layer utama yang berkaitan langsung dengan alur kerja dari REST API adalah:
1. `controller` sebagai tempat untuk mendefinisikan route dan end-point dari REST API,
2. `usecase` sebagai tempat untuk menuliskan code yang berupa fungsi logika dari aplikasi,
3. `repository` sebagai tempat untuk menyimpan code yang berkaitan dengan penyimpanan,
4. `model` sebagai inisialisasi atau representasi dari tabel-tabel dalam database, fungsi lainnya sebagai kontrak baik antara program dengan database maupun requirement untuk request dan response aplikasi

Anda dapat menggunakan sql code yang ada di folder migration, untuk dapat menggunakan REST API ini.

Endpoint, serta contoh request dan response API disimpan di file example.json

REST API ini menggunakan port 3000, jika ingin menggantinya silahkan masuk ke file main.go kemudian tedapat baris code bertuliskan `err := app.Listen(":3000")` , rubah angka 3000 dengan port angka yang available pada local anda.
